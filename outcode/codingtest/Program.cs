﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace codingtest
{
    public class Program
    {
        private static readonly Encoding LocalEncoding = Encoding.UTF8;

        /// <summary>
        /// Save ip in the memory stream
        /// </summary>
        /// <param name="ip_address"></param>
        /// <param name="unicodeEncoding"></param>
        public static void request_handled(string ip_address, UnicodeEncoding unicodeEncoding)
        {
            MemoryStream ms = new MemoryStream();
            var sw = new StreamWriter(ms, unicodeEncoding);
            try
            {
                sw.Write(ip_address);
                sw.Flush();//otherwise you are risking empty stream
            }
            finally
            {
                sw.Dispose();
            }
        }

        /// <summary>
        /// Returns a string list with the top 100 repeated ips 
        /// </summary>
        /// <returns></returns>
        public static List<String> top100()
        {
            var ipList = getFromMemory();
            //Get ip names and the number of times it repeats
            var dictionary = ipList.GroupBy(x => x)
              .Where(g => g.Count() > 1)
              .ToDictionary(x => x.Key, y => y.Count());
            //Sort dictionary by value (number of times repeated)
            Dictionary<string, int> sortedDict = (Dictionary<string, int>)(from entry in dictionary orderby entry.Value ascending select entry);

            //Pass dictionary to a list of keys 
            List<String> topIps = sortedDict.Keys.ToList();
            
            return (List<string>)topIps.Take(100);


        }
        
        public static List<String> getFromMemory()
        {
            MemoryStream stream = null;
            TextWriter writer = null;

            try
            {
                stream = new MemoryStream();

                writer = new StreamWriter(stream, LocalEncoding);

                var buffer = new byte[stream.Length];

                stream.Read(buffer, 0, (int)stream.Length);

                var settingsString = LocalEncoding.GetString(stream.ToArray());

                Console.WriteLine(settingsString);

            }
            catch (Exception ex)
            {
                // If the action cancels we don't want to throw, just return null.
            }
            finally
            {
                if (stream != null)
                    stream.Close();

                if (writer != null)
                    writer.Close();
            }

            return null;
        }
        public static void Main(string[] args)
        {
            //Today should be retrieved from 
            DateTime now = DateTime.Now;
            DateTime end = new DateTime(now.Year, now.Month, now.Day, 23, 59, 0);
            
            //If it is a new day
            if (now == end)
            {
                clear();
            }
            //request_handled(ip, uniEncoding);
            top100();
        }
    }
}
